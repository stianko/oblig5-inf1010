/*
 *  Oblig 5 INF1010 v14
 *
 *  Sudoku
 *
 *  Stian Kongsvik
 *  stiako
 *
 */

public class FilledSquare extends Square{
    FilledSquare(int value, int y, int x, Board board){
        super(value, y, x, board);
    }

    //Recursive method for solving the sudoku. Saves current solution on reaching the of the
    //linked list of Squares
    public void fillRemaining(){
        //System.out.println(value);
        if(next != null){
            //System.out.println(x + " " + y + " empty");
            next.fillRemaining();
        } else {
            board.saveSolution();
            board.solutionCounter();
        }
    }
}
