/*
 *  Oblig 5 INF1010 v14
 *
 *  Sudoku
 *
 *  Stian Kongsvik
 *  stiako
 *
 */

public class Solution{
    private int[][] solutionArray;
    private int x, y;
    private int size;

    Solution(int size){
        solutionArray = new int[size][size];
        this.size = size;
    }

    //Inserts values in solution
    public void insert(int value){
        solutionArray[y][x++] = value;
        if(x == size){
            x = 0;
            y++;
        }
    }

    public void printSolution(){
        for(int i = 0; i < size; i++){
            for(int j = 0; j < size; j++){
                System.out.print(Integer.toHexString(solutionArray[i][j]).toUpperCase() + " ");
            }
            System.out.println();
        }
        System.out.println();
    }

    public int[][] get(){
        return solutionArray;
    }
}
