/*
 *  Oblig 5 INF1010 v14
 *
 *  Sudoku
 *
 *  Stian Kongsvik
 *  stiako
 *
 */

import java.awt.event.*;

public class Controller{
    private Model model;
    private View view;
    private int solution = -1;
    private boolean solved;

    Controller(Model model, View view){
        this.model = model;
        this.view = view;

        view.addNextListener(new NextListener());
        view.addPrevListener(new PrevListener());
        view.startTimedUpdates(new TimeListener());
    }

    //Listener for "next button". Only listens when at least one solution is available.
    class NextListener implements ActionListener{
        public void actionPerformed(ActionEvent e){
            if(solved){
                solution++;
                if(solution < model.getStoredSolutionCount()){
                    view.updateLabelBoard(model.getSolution(solution));
                } else {
                    solution = 0;
                    view.updateLabelBoard(model.getSolution(solution));
                }
                view.updateCurrentSolution("" + (solution + 1));
                view.getContentPane().repaint();
            }
        }
    }

    //Listener for "previous button". Only listens when at least one solution is available.
    class PrevListener implements ActionListener{
        public void actionPerformed(ActionEvent e){
            if(solved){
                solution--;
                if(solution <= 0){
                    solution = model.getStoredSolutionCount() - 1;
                    view.updateLabelBoard(model.getSolution(solution));
                } else {
                    view.updateLabelBoard(model.getSolution(solution));
                }
                view.updateCurrentSolution("" + (solution + 1));
                view.getContentPane().repaint();
            }
        }
    }

    //Timed listener that updates number of found solutions while solution-thread is running
    class TimeListener implements ActionListener{
        public void actionPerformed(ActionEvent e){
            view.setSolutionCount();
            if(!model.t1.isAlive() && model.getStoredSolutionCount() == 0){
               view.setNoSolutions();
            } else if (model.t1.isAlive() && model.getStoredSolutionCount() == 0){
                solved = false;
            } else {
                solved = true;
            }
            view.pack();
        }
    }
}
