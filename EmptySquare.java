/*
 *  Oblig 5 INF1010 v14
 *
 *  Sudoku
 *
 *  Stian Kongsvik
 *  stiako
 *
 */

public class EmptySquare extends Square{
    EmptySquare(int y, int x, Board board){
        super(y, x, board);
    }

    //Recursive method for solving the sudoku. Saves current solution on reaching the of the
    //linked list of Squares
    public void fillRemaining(){
        for(int i = 1; i <= row.getSize(); i++){
            if(checkValue(i)){
                value = i;
                if(next != null){
                    next.fillRemaining();
                } else {
                    board.saveSolution();
                    board.solutionCounter();
                }
                value = 0;
            }
        }
    }

    //Method for checking if the int value can be put in lists
    public boolean checkValue(int v){
        if(!row.contains(v) && !column.contains(v) && !box.contains(v)){
            return true;
        }
        return false;
    }
}
