/*
 *  Oblig 5 INF1010 v14
 *
 *  Sudoku
 *
 *  Stian Kongsvik
 *  stiako
 *
 */


import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.filechooser.*;
import javax.swing.BorderFactory;
import javax.swing.border.*;
import javax.swing.Timer;

//Passive class that gets polled from View and Controller
class Model{
    private BoardParser parser;
    private Board board;
    private SudokuContainer sudokuCont;
    private int storedSolutionCount;
    private int totalSolutionCount;
    private String timer;
    private int size;
    private int[][] emptyBoard; //The problem read while parsing the file
    private int[] boxSize;
    Thread t1,t2;

    //Parses file and starts solving the problem
    public void parseFile(String name){
        parser = new BoardParser(name,1);
        board = parser.getBoard();
        setEmptyBoard();
        t1 = new Thread(new FillBoard(), "Fill-method");
        t2 = new Thread(new updateSolutionCount(), "updating solutions");
        t1.start();
        t2.start();
        size = board.getSize();
        sudokuCont = board.getContainer();
        boxSize = parser.getBoxSize();

    }

    //Makes thread for solving the problem
    private class FillBoard implements Runnable{
        public void run(){
            board.fill();
            timer = board.timer();
        }
    }

    //Makes thread for checking current number of solutions while solving
    private class updateSolutionCount implements Runnable{
        public void run(){
            while(t1.isAlive()){
                storedSolutionCount = sudokuCont.getSolutionCount();
            }
        }
    }

    private void setEmptyBoard(){
        emptyBoard = board.getEmptyBoard();
    }

    public int getSize(){
        return size;
    }

    public int[][] getSolution(int i){
        return sudokuCont.get(i);
    }

    public int getStoredSolutionCount(){
        return storedSolutionCount;
    }

    public int getTotalSolutionCount(){
        return board.getTotalSolutions();
    }

    public String getTimer(){
        return timer;
    }

    public int[][] getEmptyBoard(){
        return emptyBoard;
    }

    public int[] getBoxSize(){
        return boxSize;
    }

}

class View extends JFrame{
    private JFileChooser chooser;
    private int returnVal;
    private JLabel solutionCount = new JLabel("0");
    private Model model;
    private JLabel[] labelArray;
    private JLabel currentSolution;
    private JPanel content;
    private JPanel buttonPanel;
    private JPanel infoPanel;
    private Container boardPanels;
    private JButton nextButton, prevButton;
    private int boxX, boxY;
    private Timer updateTimer;
    private Dimension d;
    Color white = new Color(245,255,255);
    Color blue = new Color(32,188,252);
    Color lightBlue = new Color(156, 219, 253);
    Color lighterBlue = new Color(192,232,253);
    Color green = new Color(118,159,115);
    Color lightGreen = new Color(163,219,159);
    Color red = new Color(235,96,94);
    Color black = new Color(0,0,0);
    Color light = new Color(216,216,216);
    Color bg = new Color(245, 246, 246);
    Border border = BorderFactory.createLineBorder(light, 1);
    Border blackBorder = BorderFactory.createLineBorder(black, 1);
    Border noBorder = BorderFactory.createLineBorder(black, 0);
    Border emptyBorder = BorderFactory.createLineBorder(bg, 10);
    GridBagConstraints c = new GridBagConstraints();
    private int blink;

    View(Model model){
        this.model = model;

        //Main panel
        content = new JPanel();
        content.setLayout(new GridBagLayout());
        content.setBackground(bg);
        content.setBorder(emptyBorder);

        //Filechooser
        chooser = new JFileChooser();
        FileNameExtensionFilter filter = new FileNameExtensionFilter("Text-files", "txt");
        chooser.setFileFilter(filter);

        //Panel with buttons and current solution
        buttonPanel = new JPanel();
        buttonPanel.setLayout(new FlowLayout());
        buttonPanel.setBackground(bg);

        infoPanel = new JPanel();
        infoPanel.setBackground(bg);

        this.setContentPane(content);
        this.setTitle("Oblig 5 - Sudoku");
        this.pack();
        this.setResizable(false);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        //Calls for opening file and solving the problem
        openFile();
        parseFile();

        boxX = model.getBoxSize()[0];
        boxY = model.getBoxSize()[1];

        //Array for storing Labels in the board
        labelArray = new JLabel[model.getSize() * model.getSize()];
        makeLabelBoard(model.getEmptyBoard());

        //----------------Laying out Panels on main Panel
        c.gridy = 0;
        c.gridx = 0;
        c.gridwidth = 2;
        content.add(boardPanels, c);

        currentSolution = new JLabel("0");

        setSolutionCount();

        prevButton = new JButton("Previous");
        buttonPanel.add(prevButton);

        buttonPanel.add(currentSolution);

        nextButton = new JButton("Next");
        buttonPanel.add(nextButton);
        c.gridy = 1;

        content.add(buttonPanel, c);

        c.gridy = 2;
        content.add(solutionCount, c);
        //------------------------------------------------

        this.pack(); //Resizes JFrame

    }

    // Method for making board with the problem from the read file
    private void makeLabelBoard(int[][] board){
        int x = 0;
        int y = 0;
        int boxXcount = 0;
        int tmpBox = 1;
        int boxYcount = 0;
        int tmp;
        int i = 0;

        //Panel for laying out labels in a grid
        boardPanels = new JPanel(new GridLayout(model.getSize(), 0, -1, -1));
        for(JLabel label : labelArray){
            tmp = board[y][x++];
            if(tmp == 16){
                label = new JLabel(" G ");
            } else {
                label = new JLabel(" " + Integer.toHexString(tmp).toUpperCase() + " ");
            }
            d = label.getPreferredSize();
            label.setPreferredSize(new Dimension(d.width+24,d.height+24));
            if (tmp == 0){
                label.setText("   ");
            }
            labelArray[i++] = label;

            //---------------------Makes boxes--------------------------
            if(boxXcount == boxX){
                tmpBox *= -1;
                boxXcount = 0;
            }

            if(boxYcount == boxY){
                tmpBox *= -1;
                boxYcount = 0;
            }
            boxXcount++;

            if(tmpBox < 0){
                label.setBackground(white);
            } else {
                label.setBackground(lighterBlue);
            }

            if((model.getSize() & 1) != 0){
                if(x == model.getSize()){
                    tmpBox *= -1;
                }
            }

            if((boxX & 1) == 0 && boxX > boxY){
                if(x == model.getSize()){
                    tmpBox *= -1;
                }
            }

            //-------------------End of boxmaking-----------------------

            if(x == model.getSize()){
                x = 0;
                y++;
                boxYcount++;
            }

            label.setOpaque(true);
            label.setHorizontalAlignment(JLabel.CENTER);
            label.setFont(new Font("Sans",Font.PLAIN, 24));
            label.setBorder(border);
            //Sets color and font for original problem
            if(tmp != 0){
                label.setForeground(red);
                label.setFont(new Font("Sans",Font.BOLD, 24));
            }

            boardPanels.add(label);

        }
    }

    //Makes dialog box if parsing fails.
    private void parseFile(){
          try{
            model.parseFile(fileName());
          } catch (Exception e){
              int selection = JOptionPane.showConfirmDialog(this,
                  "Parsing failed. Open new file?", "Error", JOptionPane.YES_NO_OPTION);
              if(selection == 1){
                  System.out.println("Bye!");
                  System.exit(0);
              }
              openFile();
              parseFile();
          }
    }

    //Updates board with the solution as parameter
    public void updateLabelBoard(int[][] board){
        int x = 0;
        int y = 0;
        int tmp;
        for(JLabel label : labelArray){
            tmp = board[y][x++];
            if(tmp == 16){
                label.setText(" G ");
            } else {
                label.setText(" " + Integer.toHexString(tmp).toUpperCase() + " ");
            }
            if(x == model.getSize()){
                x = 0;
                y++;
            }
        }
        pack();
    }

    public void updateCurrentSolution(String s){
        currentSolution.setText(s);
    }

    private void openFile(){
        returnVal = chooser.showOpenDialog(this);
    }

    public void setSolutionCount(){
        String tmp1 = "" + model.getStoredSolutionCount() + " stored";
        String tmp2 = "" + model.getTotalSolutionCount() + " total";
        String tmp3 = "in " + model.getTimer();
        if(model.t1.isAlive()){
            tmp3 = "solving ... ";
        }
        solutionCount.setText(tmp1 + ", " + tmp2 + ", " + tmp3);
    }

    public void setNoSolutions(){
        solutionCount.setText("No solutions");
        content.remove(buttonPanel);
    }

    public String fileName(){
        if(returnVal == JFileChooser.APPROVE_OPTION){
            String name = chooser.getSelectedFile().getName();
            this.setTitle("Oblig 5  - Sudoku: " + name);
            return chooser.getSelectedFile().getAbsolutePath();
        }
        return null;
    }

    //-----Adds listeners to buttons, and a timed listener
    public void addNextListener(ActionListener nal){
        nextButton.addActionListener(nal);
    }

    public void addPrevListener(ActionListener pal){
        prevButton.addActionListener(pal);
    }

    public void startTimedUpdates(ActionListener tal){
        updateTimer = new Timer(100, tal);
        updateTimer.start();
    }
    //---------------------------------------------------
}

public class Oblig5GUI{
    public static void main(String[] args){
        Model model = new Model();
        View view = new View(model);
        Controller controller = new Controller(model, view);

        System.out.println(view.fileName());
        view.setVisible(true);

    }
}
