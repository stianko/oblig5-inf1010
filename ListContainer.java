/*
 *  Oblig 5 INF1010 v14
 *
 *  Sudoku
 *
 *  Stian Kongsvik
 *  stiako
 *
 */

//Abstract class for Boxes, Rows & Columns
public abstract class ListContainer{
    protected Square[] list;
    protected int counter = 0;

    ListContainer(int size){
        list = new Square[size];
    }

    public void set(Square s){
        list[counter++] = s;
    }

    public boolean contains(int value){
        for(int i = 0; i < list.length; i++){
            if(list[i].getValue() == value){
                return true;
            }
        }
        return false;
    }
    public int getSize(){
        return list.length;
    }
}
