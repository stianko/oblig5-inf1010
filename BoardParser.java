/*
 *  Oblig 5 INF1010 v14
 *
 *  Sudoku
 *
 *  Stian Kongsvik
 *  stiako
 *
 */

import java.util.*;
import java.io.*;

public class BoardParser{
    private Scanner sc;
    private int x,y;
    private char[] line;
    private Board board;
    private char c;

    //Contructor for writing to terminal
    BoardParser(String f){
        readFile(f);
        board.printBoard();
        board.fill();
        board.printSolutions();
    }

    //Contructor for printing solutions to file
    BoardParser(String args0, String args1){
        readFile(args0);
        board.fill();
        board.printToFile(args1);
    }

    //Contructor called by GUI
    BoardParser(String f, int v){
        readFile(f);
    }

    public Board getBoard(){
        return board;
    }

    public int[] getBoxSize(){
        return new int[]{x,y};
    }

    public void readFile(String f){
        try{
            sc = new Scanner(new File(f));
            System.out.println("Reading " + f);
            y = Integer.parseInt(sc.nextLine());
            x = Integer.parseInt(sc.nextLine());
            board = new Board(x * y, x, y);

            while(sc.hasNextLine()){
                line = sc.nextLine().toCharArray();
                for(int i = 0; i < line.length; i++){
                    if(line[i] == '.'){
                        board.insert(0);
                    } else {
                        board.insert(Character.getNumericValue(line[i]));
                    }
                }
            }
        } catch (Exception e){
            System.out.println("Parsing failed, file: " + f);
        }
        board.makeLists();
    }
}
