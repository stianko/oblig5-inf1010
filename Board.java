/*
 *  Oblig 5 INF1010 v14
 *
 *  Sudoku
 *
 *  Stian Kongsvik
 *  stiako
 *
 */


import java.util.*;
import java.io.*;

public class Board{
    private Square[][] board;
    private int area;
    private int counter = 0;
    private int line = 0;
    private int size;
    private boolean firstObjectSet = false;
    private final int X,Y;
    private int solutions;
    private SudokuContainer sudokuCont = new SudokuContainer();
    private long startTime, endTime;
    private double time;
    private int[][] emptyBoard;
    private PrintWriter out;

    Board(int size, int boxX, int boxY){
        board = new Square[size][size];
        area = size * size;
        this.size = size;
        X = boxX;
        Y = boxY;
        emptyBoard = new int[size][size];
    }

    //Inserts values in the Square[][]
    public void insert(int value){
        if(value != 0){
            board[line][counter] = new FilledSquare(value, line, counter, this);
        } else {
            board[line][counter] = new EmptySquare(line, counter, this);
        }
        //Sets next-pointers for the previous object
        if(firstObjectSet){
            if(counter == 0){
                board[line - 1][size - 1].setNext(board[line][counter]);
            } else {
                board[line][counter - 1].setNext(board[line][counter]);
            }
        }

        if(counter < size - 1){
            counter++;
        } else {
            line++;
            counter = 0;
        }
        firstObjectSet = true;
    }

    //Fills rows, columns and boxes
    public void makeLists(){
        int xCount = 0;
        int yCount = 0;
        int tmp = 0;
        //Iterates through the board, and makes and inserts Squares in
        //Boxes, Rows And Columns
        for(int i = 0; i < size; i++){
            Row r = new Row(size);
            Column c = new Column(size);
            Box b = new Box(size);
            for(int j = 0; j <  size; j++){
                r.set(board[i][j]);
                board[i][j].setRow(r);
                c.set(board[j][i]);
                board[j][i].setColumn(c);

                b.set(board[yCount][xCount]);
                board[yCount][xCount].setBox(b);
                xCount++;
                if(xCount == X + tmp){
                    xCount = tmp;
                    yCount++;
                    if(yCount == size){
                        tmp += X;
                        yCount = 0;
                        xCount = tmp;
                    }
                }
            }
        }
    }

    public int getTotalSolutions(){
        return solutions;
    }

    public void solutionCounter(){
        solutions++;
    }

    //Makes board based on the problem read by parser
    private void makeEmptyBoard(){
        int x = 0;
        int y = 0;
        emptyBoard = new int[size][size];
        Square current = board[0][0];
        while(current.getNext() != null){
            emptyBoard[y][x++] = current.getValue();
            current = current.getNext();
            if(x == size){
                x = 0;
                y++;
            }
        }
        emptyBoard[y][x] = current.getValue();
    }

    public int[][] getEmptyBoard(){
        makeEmptyBoard();
        return emptyBoard;
    }

    public String timer(){
        if(time >= 1000){
            return "" + time/1000 + " s";
        }
        return "" + time + " ms";
    }

    public SudokuContainer getContainer(){
        return sudokuCont;
    }

    public void fill(){
        startTime = System.nanoTime();
        board[0][0].fillRemaining();
        endTime = System.nanoTime();
        time = (endTime - startTime) / (double) 1000000;
    }

    public int getSize(){
        return size;
    }

    public void printBoard(){
        String hexString;
        int value;
        for(int i = 0; i < size; i++){
            for(int j = 0; j < size; j++){
                value = board[i][j].getValue();
                hexString = Integer.toHexString(value).toUpperCase();
                if(value == 0){
                    System.out.print("· ");
                } else {
                    System.out.print(hexString + " ");
                }
            }
            System.out.println();
        }
        System.out.println();
    }

    //Saves solution to the container
    public void saveSolution(){
        Solution s = new Solution(size);
        Square current = board[0][0];
        while(current.getNext() != null){
            s.insert(current.getValue());
            current = current.getNext();
        }
        s.insert(current.getValue());
        sudokuCont.insert(s);
    }

    public void printSolutions(){
        sudokuCont.printAll();
        System.out.println("Number of solutions: " + solutions + " in " + time + " ms\n");
    }

    //Saves all solution to file
    public void printToFile(String outFile){
        int[][] problem = getEmptyBoard();
        try{
            out = new PrintWriter(outFile);
            out.println("----- Original board -----");
            out.println("Box dimensions: X=" + X + ", Y=" + Y);

            for(int i = 0; i < size; i++){
                for(int j = 0; j < size; j++){
                    if(problem[i][j] == 0){
                        out.print(". ");
                    } else {
                        out.print(Integer.toHexString(problem[i][j]).toUpperCase() + " ");
                    }
                }
                out.println();
            }

            out.println("-----   Solutions    -----");
            for(int s = 0; s < sudokuCont.getSolutionCount(); s++){
                problem = sudokuCont.get(s);
                for(int i = 0; i < size; i++){
                    for(int j = 0; j < size; j++){
                        out.print(Integer.toHexString(problem[i][j]).toUpperCase() + " ");
                    }
                    out.println();
                }
                out.println();
            }
            out.print(sudokuCont.getSolutionCount() + " stored solutions, ");
            out.print(solutions + " total solutions, in " + timer());

            out.close();
        } catch(IOException e){
            System.out.println("COULD NOT WRITE TO FILE");
        }
    }

    //Prints linked squares as a test
    public void printLinked(){
        Square current = board[0][0];
        while(current.getNext() != null){
            System.out.print(Integer.toHexString(current.getValue()).toUpperCase());
            current = current.getNext();
        }
        System.out.println(Integer.toHexString(current.getValue()).toUpperCase());
        System.out.println();
    }
}
