/*
 *  Oblig 5 INF1010 v14
 *
 *  Sudoku
 *
 *  Stian Kongsvik
 *  stiako
 *
 */

public class SudokuContainer{
    private Solution[] solution = new Solution[750];
    private int counter = 0;

    public void insert(Solution s){
        if(counter < 750){
            solution[counter++] = s;
        }
    }

    public int[][] get(int i){
        return solution[i].get();
    }

    public int getSolutionCount(){
        return counter;
    }

    public void printAll(){
        for(int i = 0; i < counter; i++){
            solution[i].printSolution();
        }
    }
}
