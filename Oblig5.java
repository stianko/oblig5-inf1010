/*
 *  Oblig 5 INF1010 v14
 *
 *  Sudoku
 *
 *  Stian Kongsvik
 *  stiako
 *
 */

class Oblig5{
    public static void main(String[] args){
        Help h = new Help();
        BoardParser b;
        if (args.length < 1) {
            h.printHelp();
        } else if (args.length < 2 && args[0].contains("txt")){
            b = new BoardParser(args[0]);
        } else if (args.length == 2 && args[0].contains("txt") && args[1].contains("txt")){
            b = new BoardParser(args[0],args[1]);
        } else {
            h.printHelp();
        }
    }
}
class Help{
    public void printHelp(){
        System.out.println("Use:   java Oblig5 <infile.txt>");
        System.out.println("or:    java Oblig5 <infile.txt> <outfile.txt>");
    }
}
