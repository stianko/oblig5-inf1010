/*
 *  Oblig 5 INF1010 v14
 *
 *  Sudoku
 *
 *  Stian Kongsvik
 *  stiako
 *
 */

public abstract class Square{
    protected int value;
    protected Square next;
    protected Column column;
    protected Row row;
    protected Box box;
    protected Board board;
    protected int x,y;

    //Constructor for FilledSquare
    Square(int v,int y, int x, Board board){
        value = v;
        this.y = y;
        this.x = x;
        this.board = board;
    }

    //Contructor for EmptySquare
    Square(int y, int x, Board board){
        this.y = y;
        this.x = x;
        this.board = board;
    }

    public int getValue(){
        return value;
    }

    public String getCoordinates(){
        return x + " " + y;
    }

    public void setNext(Square s){
        next = s;
    }

    public Square getNext(){
        return next;
    }

    public void set(int v){}

    public void fillRemaining(){}

    public void setColumn(Column c){
        column = c;
    }

    public void setRow(Row r){
        row = r;
    }

    public void setBox(Box b){
        box = b;
    }
}
